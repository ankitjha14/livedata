This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## clone

## npm install

## npm start

## Default port: 3000
## for live dashboard: http://localhost:3000/live
## for register & delete order: http://localhost:3000/

## prerequisites:
####npm 5+
####node 10+

used  create-react-app to create the app.

## should be improve:
- I have used browser-database module for treating my localstorage as database so I can't load data again & again
 that's why I have to reload the page after every 5 seconds.
- In real scenario we don't have to reload page we can call loadData() for that
- should add pagination in case of more data.


can add other features such as live count & analytics.




