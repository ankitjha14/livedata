import React, {useState} from 'react';
import {saveTrade, deleteTrade} from '../dataSource.js';


function App() {
    const [userId, setUserId] = useState('');
    const [quantity, setQuantity] = useState('');
    const [price, setPrice] = useState('');
    const [orderId, setOrderId] = useState('');

    function handleClick(e) {
        e.preventDefault();
        let tradeInput = {
            userId,
            quantity: parseFloat(quantity),
            price: parseInt(price),
            tradeType: e.target.value
        };
        saveTrade(tradeInput)
    }


    function handleDelete(e) {
        e.preventDefault();
        orderId && deleteTrade(orderId)
    }

    return (

        <div style={{
            borderBottom: 1,
            width: '100%',
            marginTop: '10em',
            textAlign: 'center'
        }}>
            <h4>Register Your Order</h4>
            <form onSubmit={"sds"}>
            <span style={{
                marginRight: '1rem'
            }}>User ID</span>
                <input
                    type="text"
                    name="userId"
                    onChange={(e) => setUserId(e.target.value)}
                    value={userId}
                />
                <br/>
                <br/>
                <span style={{
                    marginRight: '1rem'
                }}>Quantity</span>
                <input
                    type="number"
                    name="quantity"
                    onChange={(e) => setQuantity(e.target.value)}
                    value={quantity}
                />
                <br/>
                <br/>
                <span style={{
                    marginRight: '1rem'
                }}>Price</span>
                <input
                    type="number"
                    name="price"
                    onChange={(e) => setPrice(e.target.value)}
                    value={price}
                />
                <br/>
                <br/>
                <input type="submit" value="BUY" style={{
                    marginRight: '2rem'
                }}
                       onClick={(e) => handleClick(e)}
                />
                <input type="submit" value="SELL"
                       onClick={(e) => handleClick(e)}
                />
            </form>
            <br/>
            <br/>
            <br/>
            <h4>Delete Your Order</h4>
            <form onSubmit={"sds"}>
            <span style={{
                marginRight: '1rem'
            }}>Order Id</span>
                <input
                    type="text"
                    name="orderId"
                    onChange={(e) => setOrderId(e.target.value)}
                    value={orderId}
                />
                <br/>
                <br/>
                <input type="submit" value="DELETE"
                       onClick={(e) => handleDelete(e)}
                />
            </form>
        </div>
    );
}

export default App;
