import React, {useEffect, useState} from 'react';
import {getTrades} from '../dataSource.js';

function Dashboard() {

    const [buyTrades, setBuyTrades] = useState([]);
    const [sellTrades, setSellTrades] = useState([]);
    const [time, setTime] = useState(Date.now());

    useEffect(() => {
        loadData();
        const interval = setInterval(() => {
            //TODO: Can also load data but I am here using localstorage-database so it is not reflecting
            window.location.reload();
            setTime(Date.now())
        }, 5000);
        return () => {
            clearInterval(interval);
        };
    }, []);

    const loadData = () => {
        getTrades().then(trades => {
            getUpdatedBuyTrades(trades);
            getUpdatedSellTrades(trades);
        });
    };

    const getUpdatedBuyTrades = (fetchedTrades) => {
        let prices = [];
        let updatedTrade = [];
        fetchedTrades.map(data => {
            const price = data.trade.price;
            if (data.trade.tradeType === 'BUY') {
                if (prices.includes(price)) {
                    updatedTrade.map(udata => {
                        if (udata.price === price) {
                            udata.quantity = udata.quantity + data.trade.quantity;
                            udata.id = udata.id + ' & order ' + data.id
                        }
                    })
                } else {
                    updatedTrade.push({id: data.id, ...data.trade});
                    prices.push(price)
                }
            }
        });
        updatedTrade.sort(function (a, b) {
            var keyA = a.price, keyB = b.price;
            if (keyA > keyB) return -1;
            if (keyA < keyB) return 1;
            return 0;
        });
        setBuyTrades(updatedTrade)
    };

    const getUpdatedSellTrades = (fetchedTrades) => {
        let prices = [];
        let updatedTrade = [];
        fetchedTrades.map(data => {
            const price = data.trade.price;
            if (data.trade.tradeType === 'SELL') {
                if (prices.includes(price)) {
                    updatedTrade.map(udata => {
                        if (udata.price === price) {
                            udata.quantity = udata.quantity + data.trade.quantity;
                            udata.id = udata.id + ' & order ' + data.id
                        }
                    })
                } else {
                    updatedTrade.push({id: data.id, ...data.trade});
                    prices.push(price)
                }
            }
        });
        updatedTrade.sort(function (a, b) {
            var keyA = a.price, keyB = b.price;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        setSellTrades(updatedTrade)
    };

    return (
        <div>

            <div style={{
                borderBottom: 1,
                width: '50%',
                marginTop: '2em',
                textAlign: 'center',
                float: 'right',
            }}>
                <table style={{
                    borderBottom: 1,
                    border: '1px solid black',
                    width: '98%',
                    float: 'right',
                    color: 'red'
                }}>
                    <h4>BUY</h4>
                    {buyTrades.length > 0 && buyTrades.map((data) =>
                        (<h5>{data.tradeType} : {data.quantity} kg for £{data.price} order {data.id}</h5>))}
                </table>
            </div>
            <div style={{
                borderBottom: 1,
                width: '50%',
                marginTop: '2em',
                textAlign: 'center',
                float: 'right',
            }}>
                <table style={{
                    borderBottom: 1,
                    border: '1px solid black',
                    width: '98%',
                    float: 'left',
                    color: 'green'
                }}>
                    <h4>SELL</h4>
                    {sellTrades.length > 0 && sellTrades.map((data) =>
                        (<h5>{data.tradeType} : {data.quantity} kg for £{data.price} order {data.id}</h5>))}
                </table>
            </div>
        </div>
    );
}

export default Dashboard;
