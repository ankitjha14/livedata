import {BrowserDatabase} from 'browser-database';

const browserDatabase = new BrowserDatabase({
    storageType: 'localStorage',
    storageKey: 'order'
});

async function getTrades() {
    let trades = [];
    try {
        trades = await browserDatabase.getAll('trades');

        console.log(trades);
    } catch (err) {
        console.error(err)
    }
    return trades;
}

function saveTrade(tradeInput) {
    browserDatabase.insert('trades', {trade: tradeInput})
        .then(row => console.log(row))
        .catch(err => console.error(err));
}

function deleteTrade(id) {
    browserDatabase.remove('trades', id)
        .then(row => console.log(row))
        .catch(err => console.error(err));
}

export {
    getTrades,
    saveTrade,
    deleteTrade
}

